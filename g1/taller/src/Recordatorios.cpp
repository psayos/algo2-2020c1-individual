#include <iostream>

using namespace std;

// Pre: 0 <= mes < 12
uint dias_en_mes(uint mes) {
    uint dias[] = {
        // ene, feb, mar, abr, may, jun
        31, 28, 31, 30, 31, 30,
        // jul, ago, sep, oct, nov, dic
        31, 31, 30, 31, 30, 31
    };
    return dias[mes - 1];
}

// Ejercicio 7, 8, 9 y 10

// Clase Fecha
class Fecha {
  public:
        Fecha(int dia, int mes);
        int dia();
        int mes();
        void incrementar_dia();


    // Completar declaraciones funciones
    #if EJ >= 9 // Para ejercicio 9
     bool operator==(Fecha o);
    #endif

  private:
    int dia_;
    int mes_;
    //Completar miembros internos
};

Fecha ::Fecha(int mes, int dia) : dia_(dia), mes_(mes) {};

int Fecha::dia() {
    return dia_;
}

int Fecha::mes() {
    return mes_;
}

bool Fecha::operator==(Fecha f){
    return this->dia() == f.dia() && this->mes() == f.mes();
}


ostream& operator<<(ostream& os, Fecha f){
    os << f.dia() << "/" << f.mes();
    return os;
}

void Fecha::incrementar_dia() {
    if (dia() == dias_en_mes(mes())) {
        dia_ = 1;
        if(mes() == 11){
            mes_ = 1;
        } else{
            mes_++;
        }
    } else {
        dia_++;
    }
}


/*
#if EJ >= 9
bool Fecha::operator==(Fecha o) {
    bool igual_dia = this->dia() == o.dia();
    // Completar iguadad (ej 9)
    return igual_dia;
}
#endif
*/


// Ejercicio 11, 12

class Horario{
public:
    Horario(unsigned int hora, unsigned int min);
        unsigned int hora();
        unsigned int min();
        bool operator==(Horario h);
        bool operator<(Horario h);

    private:
        unsigned int hora_;
        unsigned int min_;
};

Horario::Horario(unsigned int hora, unsigned int min) : hora_(hora), min_(min) {};

unsigned int Horario::hora() {
    return hora_;
}

unsigned int Horario::min() {
    return min_;
}

ostream& operator<<(ostream& os, Horario h){
    os << h.hora() << ":" << h.min();
    return os;
}

bool Horario::operator==(Horario h) {
    return (h.hora() == this->hora() &&
            h.min() == this->min());
}

bool Horario::operator<(Horario h) {
    bool res;
    if (this->hora() < h.hora()) {
        res = true;
    } else {
        if (this->hora() == h.hora() && this->min() < h.min()) {
            res = true;
        } else {
            res = false;
        }
    }
    return res;
}

// Ejercicio 13

// Clase Recordatorio

class Recordatorio{
    public:
        Recordatorio(Fecha f, Horario h, string s);
        string s();
        Fecha f();
        Horario h();
        bool operator==(Recordatorio r);
    private:
        Fecha f_;
        Horario h_;
        string s_;

};

bool Recordatorio::operator==(Recordatorio r) {
        return (r.f() == this->f() && r.h() == this->h() && r.s() == this->s());
}

Recordatorio::Recordatorio(Fecha f, Horario h, string s) : f_(f), h_(h), s_(s) {};

Fecha Recordatorio::f() {
    return f_;
}

string Recordatorio::s() {
    return s_;
}

Horario Recordatorio::h() {
    return h_;
}

ostream& operator<<(ostream& os, Recordatorio r){
    os << r.s() << " " << "@" << " " << r.f().dia() << "/" << r.f().mes() << " " << r.h().hora() << ":" << r.h().min();
    return os;
}

// Ejercicio 14

// Clase Agenda

class Agenda{
    public:
        Agenda(Fecha fecha_inicial);
        void incrementar_dia();
        void agregar_recordatorio(Recordatorio rec);
        vector<Recordatorio> recordatorios_de_hoy();
        Fecha hoy();

        // Auxiliares
        Recordatorio minimo(vector<Recordatorio> r);
        vector<Recordatorio> quitar(vector<Recordatorio> r, Recordatorio res);
        vector<Recordatorio> ordenar(vector<Recordatorio> r);

    private:
        Fecha hoy_;
        vector<Recordatorio> recordatorios_;
};

Agenda::Agenda(Fecha fecha_inicial) : hoy_(fecha_inicial), recordatorios_() {};

void Agenda::incrementar_dia() {
        hoy_.incrementar_dia();
}

void Agenda::agregar_recordatorio(Recordatorio rec) {
    recordatorios_.push_back(rec);
}

Fecha Agenda::hoy() {
    return hoy_;
}

vector<Recordatorio> Agenda::recordatorios_de_hoy() {
    vector<Recordatorio> res;
    for (Recordatorio rec : recordatorios_) {
        if (rec.f() == hoy_) {
            res.push_back(rec);
        }
    }
    return ordenar(res);
}
vector<Recordatorio> Agenda::ordenar(vector<Recordatorio> r) {
        vector<Recordatorio> res;
        vector<Recordatorio> res2 = r;
        while (res.size() < r.size()) {
            Recordatorio rec = minimo(res2);
            res.push_back(rec);
            res2 = quitar(res2, rec);
        }
        return res;
    }

Recordatorio Agenda::minimo(vector<Recordatorio> recordatorios) {
    Recordatorio rec = recordatorios[0];
    for(Recordatorio r : recordatorios){
        if(r.h() < rec.h()){
            rec = r;
        }
    }
    return rec;
}

vector<Recordatorio> Agenda::quitar(vector<Recordatorio> recordatorios, Recordatorio re) {
    vector<Recordatorio> res;
    for(Recordatorio rec : recordatorios){
        if(!(rec == re)){
            res.push_back(rec);
        }
    }
    return res;
}

ostream& operator<<(ostream& os, Agenda a) {
    os << a.hoy() << "\n" << "====="  << endl;
    for (int i = 0; i < a.recordatorios_de_hoy().size(); i++) {
     os <<  a.recordatorios_de_hoy()[i]  << endl;
    }
    return os;
}








