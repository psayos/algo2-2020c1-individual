#include "Lista.h"

Lista::Lista() : _first(NULL), _last(NULL), _long(0) {}

Lista::Lista(const Lista& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}

Lista::~Lista() {
    while (_first != NULL) {
        Nodo *n = _first;
        _first = _first->_next;
        _long--;
        delete n;
    }
}

Lista& Lista::operator=(const Lista& aCopiar) {
    Nodo* first = (aCopiar._first);
    for(int i = 0; i < (aCopiar._long);i++){
        (*this).agregarAtras(first->_data);
        first = (first)->_next;
    }
    (*this)._long = aCopiar._long;
    return *this;
}




void Lista::agregarAdelante(const int& elem) {
        Nodo *n = new Nodo(elem);
        Nodo *primero = _first;
        n->_next = primero;
        _first = n;
        _long++;
        if (primero != NULL) {
            primero-> _back = n;
        } else {
            _last = _first;
        }
    }




void Lista::agregarAtras(const int& elem) {
        Nodo *n = new Nodo(elem);
        Nodo *ultimo = _last;
        n->_back = ultimo;
        _last = n;
        _long++;
        if (ultimo != NULL) {
            ultimo -> _next = n;
        } else {
            _first = _last;
        }
    }




void Lista::eliminar(Nat i) {
        Nodo *n = _first;
        if (i == 0){
            Nodo* n = _first;
            _first = _first->_next;
            _long--;
            delete n;
        } else {
            int p = 0;
            while ( p <= i && (n != NULL)) {
                if (p != i) {
                    n = n-> _next;
                } else {
                    if (n->_back != NULL) {
                        (n->_back)->_next = n-> _next;
                    }
                    if (n->_next != NULL) {
                        (n->_next)-> _back = n-> _back;
                    }
                    delete n;
                    _long--;
                }
                p++;
            }
        }
    }


int Lista::longitud() const {
    return _long;
}

const int& Lista::iesimo(Nat i) const {
    Nodo* n = _first;
    int j = 0;
    while (j < i){
        n = n -> _next;
        j++;
    }
    return n -> _data;
}


int& Lista::iesimo(Nat i) {
    Nodo* n = _first;
    int j = 0;
    while (j < i){
        n = n -> _next;
        j++;
    }
   return n -> _data;
}





void Lista::mostrar(ostream& o) {
    // Completar
}
