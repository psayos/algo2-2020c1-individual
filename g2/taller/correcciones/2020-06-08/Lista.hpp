#include "Lista.h"




template<class T>
Lista<T>::Nodo::Nodo(const T& elem) : _valor(elem), back(NULL), next(NULL){};

template<class T>
Lista<T>::Nodo::Nodo(Nodo& n) : _valor(n._valor), back(n.back), next(n.next){};



template <typename T>
Lista<T>::Lista()
        :_length(0), _head(NULL), _end(NULL){}

template <typename T>
Lista<T>::Lista(const Lista<T>& l) : Lista() {
    //Inicializa una lista vacía y luego utiliza operator= para no duplicar el código de la copia de una lista.
    *this = l;
}





template <typename T>
Lista<T>::~Lista() {
    while (_head!=NULL){
        Nodo* n = _head;
        _head = _head->next;
        _length--;
        delete n;
    }
}


template <typename T>
Lista<T>& Lista<T>::operator=(const Lista<T>& aCopiar) {
    Nodo* first = (aCopiar._head);
    for(int i = 0; i < (aCopiar._length);i++){
        (*this).agregarAtras(first->_valor);
        first = (first)->next;
    }
    (*this)._length = aCopiar._length;
    return *this;
}


template <typename T>
void Lista<T>::agregarAdelante(const T& elem) {
    Nodo *n = new Nodo(elem);
    Nodo *primero = _head;
    n->next = primero;
    _head = n;
    _length++;
    if (primero != NULL) {
        primero-> back = n;
    } else {
        _end = _head;
    }
}

template <typename T>
void Lista<T>::agregarAtras(const T& elem) {
    Nodo *n = new Nodo(elem);
    Nodo *ultimo = _end;
    n->back = ultimo;
    _end = n;
    _length++;
    if (ultimo != NULL) {
        ultimo -> next = n;
    } else {
        _head = _end;
    }
}

template <typename T>
void Lista<T>::eliminar(Nat i) {
    Nodo *n = _head;
    if (i == 0){
         Nodo* n = _head;
        _head = _head->next;
        _length--;
        delete n;
    } else {
        int p = 0;
        while ( p <= i && (n != NULL)) {
            if (p != i) {
                n = n-> next;
            } else {
                if (n->back != NULL) {
                    (n->back)->next = n-> next;
                }
                if (n->next != NULL) {
                    (n->next)-> back = n-> back;
                }
                delete n;
                _length--;
            }
            p++;
        }
    }
}

template <typename T>
int Lista<T>::longitud() const {
    return _length;
}

template <typename T>
const T& Lista<T>::iesimo(Nat i) const {
    Nodo* n = _head;
    int j = 0;
    while (j < i){
        n = n -> next;
        j++;
    }
    const T& res = n -> _valor;
    return res;
}

template <typename T>
T& Lista<T>::iesimo(Nat i) {
    Nodo* n = _head;
    int j = 0;
    while (j < i){
        n = n -> next;
        j++;
    }
    T& res = n -> _valor;
    return res;
}

template <typename T>
void Lista<T>::mostrar(ostream& o) {
    // Completar
}

