#include <vector>

template <class T>
Conjunto<T>::Conjunto()
    // Completar
    :_raiz(NULL), length(0){}

template <class T>
Conjunto<T>::~Conjunto() { 
    // Completar
    destructorNodo(_raiz);
}

template<class T>
void Conjunto<T>::destructorNodo(Nodo* n) {
    if(n!=NULL){
        destructorNodo(n->_right);
        destructorNodo(n->_left);
        delete n;
    }
}
template <class T>
Conjunto<T>::Nodo::Nodo(const T &v) :_data(v),_left(NULL),_right(NULL){};

template <class T>
bool Conjunto<T>::pertenece(const T& clave) const {
    return perteneceAux(clave, _raiz);
}

template <class T>
bool Conjunto<T>::perteneceAux(const T& clave ,Nodo* n) const {
    bool res= false;
    if(n != NULL) {
        if ((n->_data) == clave) {
            res = true;
        } else {
            if(clave > n->_data) {
                res = perteneceAux(clave, (n->_right));
            } else {
                res = perteneceAux(clave, (n->_left));
            }
        }
    }
    return res;
}
template <class T>
void Conjunto<T>::insertar(const T& clave) {
    if(!pertenece(clave)){
        insertarAux(_raiz, clave);
    }
}

template <class T>
void Conjunto<T>::insertarAux(Nodo*& n, const T& clave) {
    if(n!=NULL){
        if((n->_data) > clave){
            insertarAux((n->_left),clave);
        }else{
            insertarAux((n->_right), clave);
        }
    }else{
        n = new Nodo(clave);
    }
}

template <class T>
void Conjunto<T>::remover(const T& clave) {
    if(pertenece(clave)){
        Nodo* padre = NULL;
        removerAux(_raiz, clave, padre);
    }
}

template <class T>
void Conjunto<T>::removerAux(Nodo* n, const T& clave, Nodo* padre) {
    if ((n->_data) == clave) {
        if ((n->_left) == NULL && (n->_right) == NULL) {
            if (padre == NULL){
                _raiz = NULL;
            }else{
                if(padre -> _right == n){
                    padre -> _right = NULL;
                }else{
                    padre -> _left = NULL;
                }
            }
            delete n;
        }else{
            if ((n->_left) == NULL && (n->_right) != NULL) {
                n->_data = minimoNodo(n->_right);
                padre = n;
                removerAux(n->_right, n->_data, padre);
            }else{
                n->_data = maximoNodo(n->_left);
                padre = n;
                removerAux(n->_left, n->_data, padre);
            }
        }
    }else{
        padre = n;
        if ((n->_data) > clave) {
            removerAux((n->_left), clave, padre);
        } else {
            removerAux((n->_right), clave, padre);
        }
    }
}


template <class T>
const T& Conjunto<T>::siguiente(const T& clave) {
    vector<Nodo*> v;
    return siguienteAux(clave, _raiz, v);
}

template <class T>
const T& Conjunto<T>::siguienteAux(const T& clave ,Nodo* n, vector<Nodo*> v) const {
    Nodo *res = n;
    if (n->_data == clave) {
        if((n->_right) != NULL) {
            res = res->_right;
            return minimoNodo(res); 
        } else {
            int i = 0;
            while((n->_data) > (v[i]->_data)){
                i++;
            };
            return v[i]->_data;
        }
    } else {
        v.push_back(res);
        if (clave > n->_data) {
            return siguienteAux(clave, (n->_right), v);
        } else {
            return siguienteAux(clave, (n->_left), v);
        }
    }
}

template <class T>
const T& Conjunto<T>::maximoNodo(typename Conjunto<T>::Nodo* n)const {
    while(n->_right != NULL){
        n = n->_right;
    }
    return (*n)._data;
}

template <class T>
const T& Conjunto<T>::minimoNodo(typename Conjunto<T>::Nodo* n)const {
    while(n->_left != NULL){
        n = n->_left;
    }
    return (*n)._data;
}

template <class T>
const T& Conjunto<T>::minimo() const {
    Nodo *res = _raiz;
    while(res->_left != NULL){
        res = res->_left;
    }
    return (*res)._data;
}

template <class T>
const T& Conjunto<T>::maximo() const {
    Nodo *res = _raiz;
    while(res->_left != NULL){
        res = res->_right;
    }
    return (*res)._data;
}

template <class T>
unsigned int Conjunto<T>::cardinal() const {
    return cardinalAux(_raiz);
}

template <class T>
unsigned int Conjunto<T>::cardinalAux(Nodo* n) const{
    unsigned int res = 0;
    if(n != NULL){
        res = 1 + cardinalAux(n->_left) + cardinalAux(n->_right);
    }
    return res;
}

template <class T>
void Conjunto<T>::mostrar(std::ostream&) const {
    assert(false);
}

